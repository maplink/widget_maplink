﻿var map = null;
var mmarker = null;
$(document).ready(function () {
    map = new Mapa();
    map.init("mapa");



    if (_lat != 0 && _lng != 0)
        buscaLocal(_lat, _lng, _zoom);
})

function Mapa() {
    var self, map;
    var statusTrafficInfo = { isRequesting: false }
    var markers = [];



    this.init = function (div, x, y) {
        self = this;

        map = new MMap2(document.getElementById(div));
        map.addControl(new GLargeMapControl());        
        if (_tile == "yes")
            map.addLayer(LBS_TRAFFIC_LAYER_2);
    }

    this.map = function (_map) {
        map = _map;
    }


    this.clearMarkers = function () {
        for (var i = 0; i < markers.length; i++) {
            map.removeMarker(markers[i]);
        }
        markers = [];
    }

    this.setCenter = function (x, y) {
        map.setCenter(new MPoint(x, y));
    }

    this.addMarker = function (marker, html) {
        if (html) {
            marker.html = html;
            GEvent.addListener(marker, "click", function () {
                this.openInfoWindowHtml(this.html);
            });
        }

        markers.push(marker);
        map.addMarker(marker);
    }
    this.addOverlay = function (overlay) {
        map.addOverlay(overlay);
    }

    this.fitBounds = function (arrayPoints) {
        var z = map.getZoomForPoints(arrayPoints);
        var b = LBS.Bounds.fromArray(arrayPoints);

        map.setZoom(z);
        map.setCenter(b.getCenter());
    }
    this.zoomToBounds = function (bounds) {
        map.zoomToExtent(bounds);
    }

    this.panTo = function (x, y, z) {
        if (z) map.setZoom(z);
        map.panTo(new GLatLng(y, x));
    }

    this.createMarker = function (x, y, html, icon, click, draggable) {
        var latlng = new GLatLng(y, x);

        lat = x;
        lng = y;

        var options = {};
        if (draggable && draggable == true) options.draggable = true;
        if (icon) options.icon = icon;

        mmarker = new GMarker(latlng, options);

        if (html)
            mmarker.html = html;

        if (click && typeof click == "function")
            GEvent.addListener(mmarker, "click", click);

        return mmarker;
    }

    this.icon = function (image, size, anchor) {
        var newImg = new Image();
        newImg.src = image;
        if (!size) size = { w: newImg.width, h: newImg.height }
        if (!anchor) anchor = { x: size.w / 2, y: size.h / 2 }
        //if (!iwPoint) iwPoint = { x: size.w/2, y: 0 };

        var icon = new MIcon();
        icon.shadow = "";
        icon.shadowSize = null;
        icon.image = image;
        icon.iconSize = new GSize(size.w, size.h);
        icon.iconAnchor = new GPoint(anchor.x, anchor.y);
        icon.infoWindowAnchor = new GPoint(anchor.x, anchor.y);

        return icon;
    }

    this.getCenter = function () {
        return map.getCenter();
    }
    this.getZoom = function () {
        return map.getZoom();
    }

    this.updateTrafficInfo = function (callback) {
        statusTrafficInfo.isRequesting = true;

        var latitude = map.center.y;
        var longitude = map.center.x;


        Common.RevGeo(latitude, longitude, function (addrLoc) {
            if (addrLoc == null) {
                statusTrafficInfo.isRequesting = false;
                return;
            }

            city = addrLoc.address.city.name != "" ? addrLoc.address.city.name : "";
            state = addrLoc.address.city.state != "" ? addrLoc.address.city.state : "";

            if (city == "" || city == null || state == "" || state == null)
                return;

            statusTrafficInfo.isRequesting = false;

            if (typeof callback == "function")
                callback(addrLoc);
        });
    }

    this.addListener = function (event, fn) {
        map.addListener(event, fn);
    }
}

function buscaLocal(y, x, z) {

    var lat, lng, zoom;

    if ($("#endereco").data('point') != null && $("#endereco").data('point') != '') {
        var dataPoint = JSON.parse($("#endereco").data('point'));
        lat = dataPoint.lat;
        lng = dataPoint.lng;
        zoom = 200;
    } else {
        lng = x;
        lat = y;
        zoom = z;
    }
    var icon = map.icon("Content/images/markers/pin_mapa_parada.png");
    map.clearMarkers();
    map.panTo(lng, lat, zoom);

    var marker = new MMarker(new MPoint(lng, lat), icon);
    map.addMarker(marker);
}