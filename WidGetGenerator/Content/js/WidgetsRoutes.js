﻿$(document).ready(function () {
    $(".button").button();
    map = new Mapa();
    map.init("mapa");

    if (_lat != 0 && _lng != 0) {
        getPlace(_lat, _lng, 15);

        var point = new MPoint(_lng, _lat);
        var af = new MWsAddressFinder();
        af.getAddress(point, function (result) {
            var data = { lat: _lat, lng: _lng };
            var dataCityState = { cidade: result.address.city.name, uf: result.address.city.state };
            $("#wEndereco2").val(result.address.street + ", " + result.address.houseNumber).attr('data-point', JSON.stringify(data)).attr('data-citystate', JSON.stringify(dataCityState));
            $("#wCidadeEstado2").val(result.address.city.name + ", " + result.address.city.state);
        });
    }

    autoComplete("#wCidadeEstado1", acType.city);
    autoComplete("#wEndereco1", acType.address);
    autoComplete("#wCidadeEstado2", acType.city);
    autoComplete("#wEndereco2", acType.address);

    $("#wCidadeEstado1").blur(function () {
        $("#wEndereco1").attr("data-citystate", JSON.stringify({ "cidade": $("#wCidadeEstado1").val().split(',')[0], "uf": $("#wCidadeEstado1").val().split(',')[1] }));
    }).change();

    $("#wCidadeEstado2").blur(function () {
        $("#wEndereco2").attr("data-citystate", JSON.stringify({ "cidade": $("#wCidadeEstado2").val().split(',')[0], "uf": $("#wCidadeEstado2").val().split(',')[1] }));
    }).change();

    $("#inverte_rota").click(function () {
        //        var _cidadeDe = $("#wCidadeEstado1").val();
        //        var _enderecoDe = $("#wEndereco1").val();
        //        var _dataPointDe = $("#wEndereco1").attr('data-point');
        //        var _cidadeOr = $("#wCidadeEstado2").val();
        //        var _enderecoOr = $("#wEndereco2").val();
        //        var _dataPointOr = $("#wEndereco2").attr('data-point');

        //        $("#wCidadeEstado1").val(_cidadeOr);
        //        $("#wCidadeEstado2").val(_cidadeDe);

        //        $("#wEndereco1").val(_enderecoOr).attr('data-point', _dataPointOr);
        //        $("#wEndereco2").val(_enderecoDe).attr('data-point', _dataPointDe);

        var destino = $('#destino').find('li');
        var origem = $('#origem').find('li');

        $('#destino li').remove();
        $('#origem li').remove();
        $('#destino').append(origem);
        $('#origem').append(destino);


        $('#destino li').find('.pin').removeClass('a').addClass('b').text('B');
        $('#origem li').find('.pin').removeClass('b').addClass('a').text('A');

        if ($("#wEndereco1").val() != "" && $("#wEndereco2").val() != "")
            doSearch();

        autoComplete("#wCidadeEstado1", acType.city);
        autoComplete("#wEndereco1", acType.address);
        autoComplete("#wCidadeEstado2", acType.city);
        autoComplete("#wEndereco2", acType.address);
    });

    $("#botao_ir").click(function () {
        doSearch();
    });
});

var map = null;
var layer = null;

function getPlace(y, x, z) {
    var icon = map.icon(_image);
    map.clearMarkers();
    map.panTo(x, y, z);

    var marker = new MMarker(new MPoint(x, y), icon);
    map.addMarker(marker);
}

function Mapa() {
    var self, map;
    var statusTrafficInfo = { isRequesting: false }
    var markers = [];
    var _rota = [];


    this.init = function (div, x, y) {
        self = this;

        map = new MMap2(document.getElementById(div));
        //        map.addControl(new GLargeMapControl());

        if (_tile == "yes")
            map.addLayer(LBS_TRAFFIC_LAYER_2);

        switch (_zControl) //tipo do controle de zoom e mapa
        {
            case "big":

                map.addControl(new GSmallMapControl());

                break;
            case "small":
                map.addControl(new GSmallMapControl());

                break;
            case 'no':
                break;
            default:
                break;
        }

        switch (_mControl) //sim ou não para map control (satélite)
        {
            case 'yes':

                map.addControl(new GMapTypeControl(), new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(5, 5)));
                break;
            case 'no':
                break;
            default:
                break;
        }
    }

    this.routeMarker = function (lat, lng, html, icon, onclick) {
        if (_rota && _rota.marker)
            map.removeMarker(_rota.marker);

        _rota.marker = this.createMarker(lng, lat, html, icon, onclick, true);

        GEvent.addListener(_rota.marker, "click", function () {
            map.closeInfoWindow();
            this.openInfoWindowHtml(this.html);
            _gaq.push(['_trackEvent', "Homepage", 'maptip', 1]);
        });

        map.addMarker(_rota.marker);
    }

    this.closeInfoWindow = function () {
        map.closeInfoWindow();
    }

    this.map = function (_map) {
        map = _map;
    }


    this.clearMarkers = function () {
        for (var i = 0; i < markers.length; i++) {
            map.removeMarker(markers[i]);
        }
        markers = [];
    }

    this.setCenter = function (x, y) {
        map.setCenter(new MPoint(x, y));
    }

    this.addMarker = function (marker, html) {
        if (html) {
            marker.html = html;
            GEvent.addListener(marker, "click", function () {
                this.openInfoWindowHtml(this.html);
            });
        }

        markers.push(marker);
        map.addMarker(marker);
    }
    this.addOverlay = function (overlay) {
        map.addOverlay(overlay);
    }

    this.fitBounds = function (arrayPoints) {
        var z = map.getZoomForPoints(arrayPoints);
        var b = LBS.Bounds.fromArray(arrayPoints);

        map.setZoom(z);
        map.setCenter(b.getCenter());
    }
    this.zoomToBounds = function (bounds) {
        map.zoomToExtent(bounds);
    }

    this.panTo = function (x, y, z) {
        if (z) map.setZoom(z);
        map.panTo(new GLatLng(y, x));
    }

    this.createMarker = function (x, y, html, icon, click, draggable) {
        var latlng = new GLatLng(y, x);

        lat = x;
        lng = y;

        var options = {};
        if (draggable && draggable == true) options.draggable = true;
        if (icon) options.icon = icon;

        mmarker = new GMarker(latlng, options);

        if (html)
            mmarker.html = html;

        if (click && typeof click == "function")
            GEvent.addListener(mmarker, "click", click);

        return mmarker;
    }

    this.icon = function (image, size, anchor) {
        var newImg = new Image();
        newImg.src = image;        
        if (!size) size = { w: newImg.width, h: newImg.height }
        if (!anchor) anchor = { x: size.w / 2, y: size.h / 2 }
        //if (!iwPoint) iwPoint = { x: size.w/2, y: 0 };

        var icon = new MIcon();
        icon.shadow = "";
        icon.shadowSize = null;
        icon.image = image;
        icon.iconSize = new GSize(size.w, size.h);
        icon.iconAnchor = new GPoint(anchor.x, anchor.y);
        icon.infoWindowAnchor = new GPoint(anchor.x, anchor.y);

        return icon;
    }

    this.getCenter = function () {
        return map.getCenter();
    }
    this.getZoom = function () {
        return map.getZoom();
    }

    this.addListener = function (event, fn) {
        map.addListener(event, fn);
    }

    this.updateTrafficInfo = function (callback) {
        statusTrafficInfo.isRequesting = true;

        var latitude = map.center.y;
        var longitude = map.center.x;


        Common.RevGeo(latitude, longitude, function (addrLoc) {
            if (addrLoc == null) {
                statusTrafficInfo.isRequesting = false;
                return;
            }

            city = addrLoc.address.city.name != "" ? addrLoc.address.city.name : "";
            state = addrLoc.address.city.state != "" ? addrLoc.address.city.state : "";

            if (city == "" || city == null || state == "" || state == null)
                return;

            statusTrafficInfo.isRequesting = false;

            if (typeof callback == "function")
                callback(addrLoc);
        });
    }

//    this.Router = function (routeType, objOrigem, objDestino, arParadas, callback) {
//        var ro = new MRouteOptions();

//        var rd = new MRouteDetails();
//        rd.descriptionType = 0;
//        rd.optimizeRoute = false;
//        rd.routeType = routeType;
//        //rd.poiRoute = ["radar"]; //"gnv", "shell", "ctf", "ipiranga""petrobras"

//        var ve = new MVehicle();
//        ve.averageSpeed = (routeType == 2 ? 5 : 70);
//        ve.tankCapacity = 42;
//        ve.averageConsumption = 13;
//        ve.fuelPrice = 2.15;
//        ve.tollFeeCat = 2;

//        var ro = new MRouteOptions();
//        ro.language = "portugues";
//        ro.vehicle = ve;
//        ro.routeDetails = rd;

//        var rs = [new MRouteStop(), new MRouteStop()];
//        /* define o ponto de origem */

//        rs[0].description = objOrigem.description;
//        var pointO = new MPoint();
//        pointO.x = objOrigem.point.x;
//        pointO.y = objOrigem.point.y;
//        rs[0].point = pointO;

//        /* define o ponto de destino */
//        rs[1].description = objDestino.description;
//        var pointD = new MPoint();
//        pointD.x = objDestino.point.x;
//        pointD.y = objDestino.point.y;
//        rs[1].point = pointD;
//        var rp = new Array;

//        var routePointAux;
//        routePointAux = new MRoutePoint();
//        routePointAux.routeStop = rs[0];
//        routePointAux.icon = this.icon("Content/images/markers/pin-verde.png");
//        rp.push(routePointAux);

//        if (arParadas != null) {
//            for (var i = 0; i < arParadas.length; i++) {
//                var rsParada = new MRouteStop();
//                rsParada.description = arParadas[i].description;

//                var pointP = new MPoint();
//                pointP.x = parseFloat(arParadas[i].point.x);
//                pointP.y = parseFloat(arParadas[i].point.y);
//                rsParada.point = pointP;

//                routePointAux = new MRoutePoint();
//                routePointAux.routeStop = rsParada;
//                routePointAux.icon = this.icon("Content/images/markers/pin-azul-" + (i + 1) + ".png");
//                rp.push(routePointAux);
//            }
//        }

//        routePointAux = new MRoutePoint();
//        routePointAux.routeStop = rs[1];
//        routePointAux.icon = this.icon("Content/images/markers/pin-vermelho.png");
//        rp.push(routePointAux);

//        this.clearRoutes();
//        rm = new MRouteMannager(map, "#0e63b6");
//        rm.destroy();
//        rm.createRoute(rp, ro, null, function (r) {
//            _rota.forEach(function (r) {
//                r.destroy();
//            });

//            if (typeof callback == "function")
//                callback(r);

//            _rota.push(rm);
//        });
    //    }

    this.Router = function (routeType, objOrigem, objDestino, arParadas, callback) {
        this.clearOverlays();
        this.removeAllMarkers();

        var ro = new MRouteOptions();

        var rd = new MRouteDetails();
        rd.descriptionType = 0;
        rd.optimizeRoute = false;
        rd.routeType = parseFloat(routeType);
        //rd.poiRoute = ["radar"]; //"gnv", "shell", "ctf", "ipiranga""petrobras"

        var ve = new MVehicle();
        ve.averageSpeed = (routeType == 2 ? 5 : 70);
        ve.tankCapacity = 42;
        ve.averageConsumption = 11;
        ve.fuelPrice = 2.80;
        ve.tollFeeCat = 2;

        var ro = new MRouteOptions();
        ro.language = "portugues";
        ro.vehicle = ve;
        ro.routeDetails = rd;

        var rs = [new MRouteStop(), new MRouteStop()];
        /* define o ponto de origem */

        rs[0].description = objOrigem.description;
        var pointO = new MPoint();
        pointO.x = objOrigem.point.x;
        pointO.y = objOrigem.point.y;
        rs[0].point = pointO;

        /* define o ponto de destino */
        rs[1].description = objDestino.description;
        var pointD = new MPoint();
        pointD.x = objDestino.point.x;
        pointD.y = objDestino.point.y;
        rs[1].point = pointD;
        var rp = [];

        var routePointOrigem = new MRoutePoint();
        routePointOrigem.routeStop = rs[0];
        routePointOrigem.icon = this.icon("Content/images/markers/pin-verde.png", { w: 44, h: 44 });

        var af = new MWsAddressFinder();
        var point = new MPoint();
        point.x = routePointOrigem.routeStop.point.x;
        point.y = routePointOrigem.routeStop.point.y;

        var descricaoOrigem = "";

        af.getAddress(point, function (o) {
            descricaoOrigem += "<div itemtype='http://schema.org/PostalAddress'>";
            descricaoOrigem += "  <div>";
            descricaoOrigem += "      <strong class=\"local title\">" + o.address.street + ", " + o.address.houseNumber + "</strong><br/>";
            descricaoOrigem += "      <address>";
            descricaoOrigem += "          <span class=\"cidade_estado\">" + o.address.district + ", " + o.address.city.name + ", " + o.address.city.state + "</span>";
            descricaoOrigem += "          <span class=\"cidade_estado\">" + o.address.zip + "</span>";
            descricaoOrigem += "      </address>";
            descricaoOrigem += "  </div>";
            descricaoOrigem += "</div>";

            routePointOrigem.marker.description = descricaoOrigem;
            routePointOrigem._htmlMarker = descricaoOrigem;
        });


        routePointOrigem.marker = this.createMarker(routePointOrigem.routeStop.point.x, routePointOrigem.routeStop.point.y, routePointOrigem.routeStop.description, this.icon("Content/images/markers/pin-verde.png", { w: 44, h: 44 }),
        function (obj, _mmarker) {
            map.openInfoWindow(obj.latlng, _mmarker._htmlMarker, {
                panMap: true,
                pixelOffset: new LBS.Pixel(0, -15)
            });
        }, true);

        rp.push(routePointOrigem);

        if (arParadas != null) {
            for (var i = 0; i < arParadas.length; i++) {
                var rsParada = new MRouteStop();
                rsParada.description = arParadas[i].description;

                var pointP = new MPoint();
                pointP.x = parseFloat(arParadas[i].point.x);
                pointP.y = parseFloat(arParadas[i].point.y);
                rsParada.point = pointP;

                var _routeId = ['routePointAux', i].join('');
                window[_routeId] = new MRoutePoint();
                window[_routeId].routeStop = rsParada;

                var af = new MWsAddressFinder();

                point.x = window[_routeId].routeStop.point.x;
                point.y = window[_routeId].routeStop.point.y;

                window[_routeId].marker = this.createMarker(pointP.x, pointP.y, window[_routeId].routeStop.description, this.icon("Content/images/markers/pin-azul-" + (i + 1) + ".png", { w: 44, h: 44 }),
                function (obj, _mmarker, _x, _y, _html) {
                    var fnShowMarker = function (obj, _mmarker) {
                        map.openInfoWindow(obj.latlng, _mmarker._htmlMarker, {
                            panMap: true,
                            pixelOffset: new LBS.Pixel(0, -15)
                        });
                    };

                    if (!_mmarker._htmlMarker) {
                        var _point = new MPoint();
                        _point.x = _x;
                        _point.y = _y;

                        (new MWsAddressFinder()).getAddress(_point, function (o) {
                            var descricaoParada = "";
                            descricaoParada += "<div itemtype='http://schema.org/PostalAddress'>";
                            descricaoParada += "  <div>";
                            descricaoParada += "      <strong class=\"local title\">" + o.address.street + ", " + o.address.houseNumber + "</strong><br/>";
                            descricaoParada += "      <address>";
                            descricaoParada += "          <span class=\"cidade_estado\">" + o.address.district + ", " + o.address.city.name + ", " + o.address.city.state + "</span>";
                            descricaoParada += "          <span class=\"cidade_estado\">" + o.address.zip + "</span>";
                            descricaoParada += "      </address>";
                            descricaoParada += "  </div>";
                            descricaoParada += "</div>";

                            _mmarker._htmlMarker = descricaoParada;

                            fnShowMarker(obj, _mmarker);
                        });
                    } else {
                        fnShowMarker(obj, _mmarker);
                    };
                }, true);

                rp.push(window[_routeId]);
            }
        }

        var routePointDestino = new MRoutePoint();
        routePointDestino.routeStop = rs[1];

        var descricaoDestino = "";
        var af = new MWsAddressFinder();
        var point = new MPoint();
        point.x = routePointDestino.routeStop.point.x;
        point.y = routePointDestino.routeStop.point.y;

        af.getAddress(point, function (o) {
            descricaoDestino += "<div itemtype='http://schema.org/PostalAddress'>";
            descricaoDestino += "  <div>";
            descricaoDestino += "      <strong class=\"local title\">" + o.address.street + ", " + o.address.houseNumber + "</strong><br/>";
            descricaoDestino += "      <address>";
            descricaoDestino += "          <span class=\"cidade_estado\">" + o.address.district + ", " + o.address.city.name + ", " + o.address.city.state + "</span>";
            descricaoDestino += "          <span class=\"cidade_estado\">" + o.address.zip + "</span>";
            descricaoDestino += "      </address>";
            descricaoDestino += "  </div>";
            descricaoDestino += "</div>";

            routePointDestino.marker.description = descricaoDestino;
            routePointDestino._htmlMarker = descricaoDestino;
        });


        routePointDestino.marker = this.createMarker(routePointDestino.routeStop.point.x, routePointDestino.routeStop.point.y, routePointDestino.routeStop.description, this.icon("Content/images/markers/pin-vermelho.png", { w: 44, h: 44 }),
        function (obj, _mmarker) {
            map.openInfoWindow(obj.latlng, _mmarker._htmlMarker, {
                panMap: true,
                pixelOffset: new LBS.Pixel(0, -15)
            });
        }, true);

        rp.push(routePointDestino);


        this.clearRoutes();
        rm = new MRouteMannager(map, "#0e63b6");
        rm.destroy();
        var _cb = function (r, b) {
            _rota.forEach(function (r) {
                r.destroy();
            });
            if (typeof callback == "function") {
                callback(r, b);
            };
            _rota.push(rm);
        };
        rm.createRoute(rp, ro, null, {
            onSuccess: function (r) {
                _cb(r, true);
            },
            onError: function (r) {
                _cb(r, false);
            }
        });
    }

    this.clearRoutes = function () {
        _rota.forEach(function (r) {
            r.destroy();
            r = null;
        });

        _rota = [];

    }

    this.clearOverlays = function () {
        map.clearOverlays();
    };
    this.removeAllMarkers = function () {
        map.removeAllMarkers();
    };
}

var map = null;

function doSearch(callback) {    

    var routeDescrition = function (r) {
        map.clearMarkers();

        var commandToCSS = function (c) {
            if (/permane.a.*esquerda/i.test(c) || /se.quede.en.la.izquierda/i.test(c) || /bear.left/i.test(c))		                //permaneca a esquerda?
                return "mantenhaEsquerda";
            else if (/permane.a.*direita/i.test(c) || /se.quede.en.la.derecha/i.test(c) || /bear.right/i.test(c))	                //permaneca a direita?
                return "mantenhaDireita";
            else if (/vire.*esquerda/i.test(c) || /doble.a.la.izquierda/i.test(c) || /turn.left/i.test(c))			                //vire a esquerda?
                return "vireEsquerda";
            else if (/vire.*direita/i.test(c) || /doble.a.la.derecha/i.test(c) || /turn.right/i.test(c))			                //vire a direita?
                return "vireDireita";
            else if (/curva.*acentuada.*esquerda/i.test(c) || /fuerte.curva.a.la.izquierda/i.test(c) || /make.sharp.left/i.test(c))	//curva acentuada a esquerda?
                return "vireEsquerdaFechada";
            else if (/curva.*acentuada.*direita/i.test(c) || /fuerte.curva.a.la.derecha/i.test(c) || /make.sharp.right/i.test(c))	//curva acentuada a esquerda?
                return "vireDireitaFechada";
            else if (/retorno/i.test(c) || /vuelte/i.test(c) || /back/i.test(c))	                                                //curva acentuada a esquerda?
                return "retorno";
            else
                return "sigaEmFrente";
        };

        var sourceToCSS = function (c) {
            if (/radar/i.test(c))
                return "radar";
            else
                return "posto";
        }


        // Montar box com resumo da rota.
        if (r.routeTotals.totalDistance != 0)
            $(".box.resumoRota .distancia em").text(r.routeTotals.totalDistance).parent().show().removeClass('hide');
        else
            $(".box.resumoRota .distancia").hide();

        // Montar lista
        var ul = Common.createElement("ul");                
        var ul_acumulador = Common.createElement("ul");

        var li = Common.createElement("li", { "css": "parada origem" });
        var pinA = Common.createElement("div", { "css": "pin a" }, { text: "A" });
        var addr = Common.createElement("strong", { "css": "address" }, { text: r.routeSummary[0].description });

        li.appendChild(pinA);
        li.appendChild(addr);
        ul.appendChild(li);


//        $.each(r.segDescription, function (i, seg) {
//            if (i == 0 || i == r.segDescription.length - 1)
//                return true;

//            var _distance = (seg.distance != 0 ? (seg.distance < 1000 ? Math.round(seg.distance * 1000) + "m" : Math.round(seg.distance) + "km") : "")

//            if (seg.distance > 0)
//                li = createTableStep(
//                    { "css": commandToCSS(seg.command), "text": seg.distance, "title": "" },
//                    { "text": seg.description },
//                    { "text": _distance },
//                    { "point": seg.point, onclick: function () {
//                        var point = JSON.parse($(this).attr("data-point"));
//                        map.setCenter(point.x, point.y, 16);

//                        map.closeInfoWindow();
//                        map.routeMarker(point.y, point.x, "", map.icon("../images/markers/pin_mapa_parada.png"));
//                    }
//                    });
//            else {
//                var obj = createTableStop(seg.description, {
//                    point: seg.point, onclick: function () {
//                        var point = JSON.parse($(this).attr("data-point"));
//                        var i = parseInt($(this).attr("data-i")) + 1;
//                        map.setCenter(point.x, point.y, 16);

//                        map.closeInfoWindow();
//                        map.routeMarker(point.y, point.x, "", map.icon("../images/markers/pin-azul-" + i + ".png"));
//                    }
//                });

//                if (obj !== null)
//                    li = obj;
//                else
//                    return true;
//            }


//            ul.appendChild(li);

//            // POI
//            if (seg.poiRouteDetails)
//                $.each(seg.poiRouteDetails, function (x, poi) {
//                    var css = sourceToCSS(poi.source);
//                    var poiName, html, icon;

//                    if (css == "radar") {
//                        poiName = poi.name;
//                        html = "<div><strong>" + poi.name + "</strong><br/>" + poi.addressInfo + "</div><p/><strong>";
//                        icon = map.icon("../images/markers/pin-cameras.png", { h: 39, w: 35 }, { x: 2, y: 35 });
//                    } else {
//                        poiName = "Auto Posto " + poi.name;
//                        html = "<strong>" + poi.name + "</strong><br/>" + poi.addressInfo;
//                        icon = map.icon("../images/markers/pin-postos.png", { h: 52, w: 52 }, { x: 26, y: 34 });
//                    }

//                    li = createTableStep(
//                        { "css": css, "text": "km", "title": poi.addressInfo }, // direction
//                        {"text": poiName }, //orientation
//                        {"text": " " }, // small
//                        {"point": poi.point, "html": html, onclick: function () {
//                            var point = JSON.parse($(this).attr("data-point"));
//                            map.setCenter(point.x, point.y, 16);
//                            map.closeInfoWindow();

//                            map.routeMarker(point.y, point.x, html, icon);
//                        } //data
//                    });

//                    ul.appendChild(li);
//                });
        //        });

        var clearedSegs = clearSegs(r.segDescription);

        $.each(clearedSegs, function (i, seg) {
            if (i == 0 || i == r.segDescription.length - 1)
                return true;

            var isMatch = (!/Parada/i.test(seg.command) && seg.description == clearedSegs[i - 1].description);
            var _description = (isMatch ? seg.city.name + ", " + seg.city.state : (seg.description == "" ? seg.city.name + ", " + seg.city.state : seg.description));
            var _point = JSON.stringify(seg.point);

            if (seg.distance > 0) {
                var _command = (isMatch ? parseRoadType(clearedSegs[i - 1].roadType, seg.roadType) : commandToCSS(seg.command));
                var _distance = (seg.distance != 0 ? (seg.distance > 1 ? Math.round(seg.distance * 100) / 100 + "km" : Math.round(seg.distance * 1000) + "m") : "")


                if (seg.tollFee == null) {
                    li = $("<li/>").addClass("step").attr("data-point", _point)
                    .append($("<span/>").addClass(isMatch ? "pista" : "direction").append($("<i/>").addClass(_command).html(isMatch ? seg.roadType : "")))
                    .append($("<p/>").addClass("orientation").html(_description))
                    .append($("<small/>").addClass("meter").html(_distance))
                    .click(function () {
                        var point = JSON.parse($(this).attr("data-point"));
                        map.setCenter(point.x, point.y, 14);

                        map.closeInfoWindow();
                        map.routeMarker(point.y, point.x, "", map.icon("images/markers/pin_mapa_parada.png"));
                    });
                } else {
                    li = $("<li/>").addClass("step").attr("data-point", _point).attr("data-tollbox", JSON.stringify(seg.tollFeeDetails))
                    .append($("<span/>").addClass("direction").append($('<i class="pedagio"></i>')))
                    //.append($("<span/>").addClass("direction").append($("<i/>").addClass(_command)))
                    .append($("<p/>").addClass("orientation").html(seg.tollFeeDetails.name))
                    .append($("<small/>").addClass("meter").html(_distance))
                    .click(function () {
                        var point = JSON.parse($(this).attr("data-point"));
                        var tollDetails = JSON.parse($(this).attr("data-tollbox"));

                        map.setCenter(point.x, point.y, 14);

                        var html = $("<div/>").addClass("infoWin")
                            .append(
                                $("<div/>").addClass("local title")
                                    .append($("<p/>").addClass("nome").html(tollDetails.name)))
                            .append($("<address/>").addClass("nome").html(tollDetails.address))
                            .append($("<div/>").addClass("details")
                                .append($("<p/>").addClass("nome")
                                    .append($("<strong/>").html("Concessionária: "))
                                    .append($("<b/>").html(tollDetails.concession))
                                .append($("<p/>").addClass("nome")
                                    .append($("<strong/>").html("Telefone: ").addClass("clGray"))
                                    .append($("<b/>").html(tollDetails.phone).addClass("clGray")))
                                .append($("<p/>").addClass("nome")
                                    .append($("<strong/>").html("Preço: ").addClass("clGray"))
                                    .append($("<b/>").html(formatCurrency(tollDetails.price)).addClass("clGray")))
                                .append($("<p/>").addClass("nome")
                                    .append($("<strong/>").html("Adicional por eixo: ").addClass("clGray"))
                                    .append($("<b/>").html(formatCurrency(tollDetails.pricePerAxle)).addClass("clGray")))
                                )
                            );


                        map.closeInfoWindow();
                        map.routeMarker(point.y, point.x, html[0].outerHTML, map.icon("../images/markers/pin-pedagios.png", { w: 32, h: 32 }));
                    });
                }

                if (isMatch) {
                    $(ul_acumulador).append(li);
                    li = null;
                } else {
                    if ($(ul_acumulador).find("li").length > 0) {
                        $(ul).find("li").last().unbind("click").append(ul_acumulador);
                        ul_acumulador = Common.createElement("ul");
                    }
                }

            } else {
                var obj = null;

                if (/Parada/i.test(seg.command)) {
                    var stopIndex = seg.command.replace(/\D+/g, "");
                    obj = $("<li/>").addClass("step").attr("data-point", _point).attr("data-i", stopIndex)
                    .append($("<span/>").addClass("pin").append($("<i/>").html(stopIndex)))
                    .append($("<strong/>").addClass("address").html(_description))
                    .append($("<small/>").addClass("meter").html(""))
                    .click(function () {
                        var point = JSON.parse($(this).attr("data-point"));
                        var i = parseInt($(this).attr("data-i")) + 1;
                        map.setCenter(point.x, point.y, 14);

                        map.closeInfoWindow();
                        map.routeMarker(point.y, point.x, "", map.icon("../images/markers/pin-azul-" + i + ".png"));
                    });
                }

                if (obj !== null)
                    li = obj;
                else
                    li = null;
            }

            if (li != null)
                $(ul).append(li);
            //ul.appendChild(li);

            if (seg.tollFee != null)
                var a = 1;

            // POI
            if (seg.poiRouteDetails)
                $.each(seg.poiRouteDetails, function (x, poi) {
                    var css = sourceToCSS(poi.source);
                    var poiName, html, icon;

                    if (css == "radar") {
                        poiName = poi.name;
                        html = "<div><strong>" + poi.name + "</strong><br/>" + poi.addressInfo + "</div><p/><strong>";
                        icon = map.icon("../images/markers/pin-cameras.png", { h: 39, w: 35 }, { x: 2, y: 35 });
                    } else {
                        poiName = "Auto Posto " + poi.name;
                        html = "<strong>" + poi.name + "</strong><br/>" + poi.addressInfo;
                        icon = map.icon("../images/markers/pin-postos.png", { h: 52, w: 52 }, { x: 26, y: 34 });
                    }

                    li = createTableStep(
                    { "css": css, "text": "km", "title": poi.addressInfo }, // direction
                    {"text": poiName }, //orientation
                    {"text": " " }, // small
                    {"point": poi.point, "html": html, onclick: function () {
                        var point = JSON.parse($(this).attr("data-point"));
                        map.setCenter(point.x, point.y, 16);
                        map.closeInfoWindow();

                        map.routeMarker(point.y, point.x, html, icon);
                    } //data
                });

                ul.appendChild(li);
            });
        });

        var li = Common.createElement("li", { "css": "parada destino" });
        var pinB = Common.createElement("div", { "css": "pin b" }, { text: "B" });
        addr = Common.createElement("strong", { "css": "address" }, { text: r.routeSummary[1].description });
        li.appendChild(pinB);
        li.appendChild(addr);


        ul.appendChild(li);

        $(".steps").html("").append(ul).parent().show().removeClass('hide');
    }


    var routeType = 1;

    var arParadas = [];

    var pt = JSON.parse($('#origem li').find('.address').attr("data-point"));
    // var pt = JSON.parse($("#wEndereco1").attr("data-point"));
    arParadas.push({
        //description: $("#wEndereco1").val(),
        description: $('#origem li').find('.address').val(),
        point: { x: parseFloat(pt.lng), y: parseFloat(pt.lat) }
    });

    var pt2 = JSON.parse($('#destino li').find('.address').attr("data-point"));
   // var pt2 = JSON.parse($("#wEndereco2").attr("data-point"));
    arParadas.push({
        //description: $("#wEndereco2").val(),
        description: $('#destino li').find('.address').val(),
        point: { x: parseFloat(pt2.lng), y: parseFloat(pt2.lat) }
    });
    

    map.Router(routeType /* mais curta */,
            arParadas[0], /* origem */
            arParadas[arParadas.length - 1], /* destino */
            (arParadas.length > 2 ? arParadas.slice(1, arParadas.length - 1) : null), /* paradas */
            routeDescrition);

}

var createTableStep = function (direction, orientation, small, data) {
    var li = Common.createElement("li", { "css": "step" });

    var span = Common.createElement("span", { "css": "direction" });
    var iDirection = Common.createElement("i", { "css": direction.css }, { text: direction.text });
    iDirection.title = direction.title;
    span.appendChild(iDirection);

    var p = Common.createElement("p", { "css": "orientation" }, { text: orientation.text });

    var small = Common.createElement("small", { "css": "meter" }, { text: small.text });

    li.appendChild(span);
    li.appendChild(p);
    li.appendChild(small);

    $(li)
        .attr("data-point", JSON.stringify(data.point))
        .click(data.onclick);

    return li;
}

var parseRoadType = function (anterior, atual) {
    if ((anterior == null || anterior == "") && (atual && atual != ""))
        return atual;
    else if ((atual == null || atual == "") && (anterior && anterior != ""))
        return anterior;
    else if ((atual && atual != "") && (anterior && anterior != ""))
        return anterior.toLowerCase() + "_" + atual.toLowerCase();

    return "";
}

var clearSegs = function (obj) {
    var arrayAux = [];
    //var arrayBkp = obj;
    var aux = 1;
    for (var i = 0; i < obj.length; i++) {
        //se não for o último trecho
        if (i < obj.length - 1) {
            if (obj[i].description == obj[i + 1].description &&
                obj[i].roadType == obj[i + 1].roadType &&
                obj[i].city.name == obj[i + 1].city.name &&
                obj[i].publicTrans == obj[i + 1].publicTrans &&
                obj[i + 1].tollFee == null) {

                obj[i].distancia = (parseFloat(obj[i].distancia) + parseFloat(obj[i + 1].distancia)).toFixed(2);
                obj.splice(i + 1, 1);
                i--;
            }
        }
    }
    return obj;
}

var createTableStop = function (address, data) {
    var index = -1;


    if ($("#wEndereco2").val().indexOf(address) > -1)
        index = 1;
    else
        index = 0;

    if (index == -1)
        return null;


    var li = Common.createElement("li", { "css": "step" });

    var span = Common.createElement("span", { "css": "pin" });
    var i = Common.createElement("i", null, { "text": (index + 1) });
    span.appendChild(i);

    var p = Common.createElement("p", { "css": "orientation" }, { text: address });

    var small = Common.createElement("small", { "css": "meter" }, { text: "" });

    li.appendChild(span);
    li.appendChild(p);
    li.appendChild(small);

    $(li)
        .attr("data-point", JSON.stringify(data.point))
        .attr("data-i", index)
        .click(data.onclick);

    return li;
}