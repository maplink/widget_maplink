﻿/// <reference path="jquery-1.5.1-vsdoc.js"/>

var acType = {
    address: "a",
    city: "c"
};

function autoComplete(obj, type, callback) {
    $(obj).autocomplete({
        /*autoFocus: true,*/
        source: function (request, response) {
            var _city = $(obj).attr("data-citystate");
            if (typeof _city == "string") {
                var tmp = $(obj).attr("data-citystate");
                if (tmp) {
                    _city = JSON.parse(tmp);
                };
            };

            var reqData = {
                term: request.term.indexOf(" - ") != -1 ? request.term.substr(0, request.term.indexOf(" - ")) : request.term,
                max: 10
            };

            if (type == acType.address)
                reqData.city = _city.cidade + ", " + _city.uf;

            $.ajax({
                url: _urlAutoComplete,
                data: reqData,
                beforeSend: function () {
                    $('.loading').removeClass('hide').show();
                },
                success: function (data) {
                    $('.loading').hide();
                    var objData = JSON.parse(data);

                    if (objData.length == 0) {
                        var tmpData = {
                            City: _city == undefined ? "Nenhum resultado" : _city.cidade,
                            District: null,
                            HouseNumber: "",
                            Latitude: "0",
                            Longitude: "0",
                            State: _city == undefined ? "" : _city.uf,
                            Street: "<em>Não foi encontrado</em> " + reqData.term,
                            Uf: null,
                            Zip: null
                        };

                        var hasCity = $(obj).parent().find('.city');
                        var cityHasPoint = hasCity.data('point');
                        if (hasCity.length != 0 && !cityHasPoint) {
                            tmpData.Street = 'Preencha o campo "Cidade, UF"';
                        };

                        objData.push(tmpData);

                        data = null;
                    }

                    response($.map(objData, function (item) {
                        var labelHTML = (type == acType.address ? item.Street + (item.HouseNumber != "" ? ', ' + item.HouseNumber : "") /*+ " - <em>" + _city.cidade + ", " + _city.uf + "</em>"*/ : item.City + ", " + item.State);
                        var label = (type == acType.address ? item.Street + (item.HouseNumber != "" ? ', ' + item.HouseNumber : "") /*+ " - " + _city.cidade + ", " + _city.uf*/ : item.City + (item.State == "" ? "" : ", " + item.State));

                        return {
                            label: label,
                            html: labelHTML,
                            value: label,
                            point: JSON.stringify({ lat: item.Latitude, lng: item.Longitude }),
                            street: item.Street,
                            number: item.HouseNumber,
                            city: (item.City == null) ? _city.cidade : item.City,
                            state: (item.State == null) ? _city.uf : item.State,
                            raw: data
                        }
                    }))
                }
            })
        },
        minLength: 3,
        select: function (event, ui) {
            if (ui.item.raw == undefined)
                return false;

            if (type == acType.city) {
                $(obj)
                    .attr("data-citystate", JSON.stringify({ "cidade": ui.item.city, "uf": ui.item.state }))
                    .attr("data-point", ui.item.point)
                    .parent().find(".address")
                        .attr("data-citystate", JSON.stringify({ "cidade": ui.item.city, "uf": ui.item.state }))
                        .attr("data-point", ui.item.point)
                        .attr("data-strcity", ui.item.label)
                        .val('');
            } else {
                $(obj)
                    .attr("data-point", ui.item.point)
            }

            if (callback && typeof callback == "function") {
                callback(obj, type, ui);
            };
        }
    })
    .data("autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.html + "</a>")
            .appendTo(ul);
    };
}