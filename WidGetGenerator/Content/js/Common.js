﻿/**
* Copyright (c) Mozilla Foundation http://www.mozilla.org/
* This code is available under the terms of the MIT License
*/
if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun /*, thisp*/) {
        var len = this.length >>> 0;
        if (typeof fun != "function") {
            throw new TypeError();
        }

        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this) {
                var val = this[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, this)) {
                    res.push(val);
                }
            }
        }

        return res;
    };
}

if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fun /*, thisp*/) {
        var len = this.length >>> 0;
        if (typeof fun != "function") {
            throw new TypeError();
        }

        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this) {
                fun.call(thisp, this[i], i, this);
            }
        }
    };
}

if (!Array.prototype.avg) {
    Array.prototype.avg = function (int) {
        var av = 0;
        var cnt = 0;
        var len = this.length;
        for (var i = 0; i < len; i++) {
            var e = +this[i];
            if (!e && this[i] !== 0 && this[i] !== '0') e--;
            if (this[i] == e) { av += e; cnt++; }
        }

        var avg = av / cnt;
        return (int ? parseInt(avg) : avg);
    }
}

if (!String.prototype.toCapitalize) {
    String.prototype.toCapitalize = function () {
        return this.toLowerCase().replace(/^.|\s\S/g, function (a) { return a.toUpperCase(); });
    }
}
if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    }
}

if (!String.prototype.removerAcentos) {
    String.prototype.removerAcentos = function () {
        var rtn = this.replace(/ã|â|á|à/g, "a");
        rtn = rtn.replace(/Ã|Â|Á|À/g, "A");
        rtn = rtn.replace(/ó|õ|ô/g, "o");
        rtn = rtn.replace(/Ó|Õ|Ô/g, "O");
        rtn = rtn.replace(/í/g, "i");
        rtn = rtn.replace(/Í/g, "I");
        rtn = rtn.replace(/é|ê/g, "e");
        rtn = rtn.replace(/É|Ê/g, "E");
        rtn = rtn.replace(/ç/g, "ç");
        rtn = rtn.replace(/Ç/g, "Ç");
        return rtn;
    }
}

if (!Number.prototype.toRad) {
    Number.prototype.toRad = function () {
        return this * 180 / Math.PI;
    };
}

var Common = {
    RevGeo: function (lat, lng, callback) {
        if (lat == 0 || lng == 0 || lat == null || lng == null)
            if (typeof callback == "function") callback(null);

        var point = new MPoint(lng, lat);
        var af = new MWsAddressFinder();
        af.getAddress(point, function (addrLoc) {

            if (typeof callback == "function")
                callback(addrLoc);
        });
    },

    Geocode: function (uf, city, address, number) {

    },


    Geolocate: function (callback) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
            function (p) { // success
                var X = p.coords.longitude;
                var Y = p.coords.latitude;

                $.ajax({
                    type: 'POST',
                    url: '/Assets/RevGeo',
                    data: { lat: Y, lng: X },
                    success: function (s) {
                        if (typeof callback == "function")
                            callback(s);
                    }
                });
            },
            function (e) { //error
                if (typeof callback == "function")
                    callback(null);
            },
            { timeout: 2000 }
        );
        }
        else {
            if (typeof callback == "function")
                callback(null);
        }
    },

    createElement: function (tag, params, text) {
        var el = document.createElement(tag);

        if (params) {
            if (params.css) el.className = params.css;
            if (params.src) el.src = params.src;
            if (params.type) el.type = params.type;
            if (params.href) el.href = params.href;
            if (params.target) el.target = params.target;
            if (params.citystate) el.setAttribute("data-citystate", params.citystate);
            if (params.title) el.setAttribute("title", params.title);
            if (params.onclick) {
                if (typeof params.onclick == "function")
                    el.addEventListener("click", params.onclick, true);
                else
                    el.setAttribute("onclick", params.onclick);
            }
        }

        if (text) {
            if (text.text)
                $(el).text(text.text);
            else
                $(el).html(text.html);
        }

        return el;
    },

    evt2latlng: function (e) {
        var _m = ___LBSAPI.CurrentMap;

        var element = _m.viewPortDiv;
        if (!element.lefttop) {
            element.lefttop = [(document.documentElement.clientLeft || 0), (document.documentElement.clientTop || 0)];
        }
        if (!element.offsets) {
            element.offsets = LBS.Util.pagePosition(element);
        }

        var client = {
            x: (e.originalEvent ? e.originalEvent.clientX : e.clientX),
            y: (e.originalEvent ? e.originalEvent.clientY : e.clientY)
        }

        var px = new LBS.Pixel(client.x - element.offsets[0] - element.lefttop[0], client.y - element.offsets[1] - element.lefttop[1]);
        var latlng = _m.getLatLngFromPixel(px);

        return latlng;
    },

    distanceBetweenPts: function (p1, p2) {
        var lat1 = parseFloat( p1.lat);
        var lng1 = parseFloat(p1.lng);
        var lat2 = parseFloat(p2.lat);
        var lng2 = parseFloat(p2.lng);
        
        var R = 6371; // km
        var dLat = (lat2 - lat1).toRad();
        var dLon = (lng2 - lng1).toRad();
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
          Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
          Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }
}